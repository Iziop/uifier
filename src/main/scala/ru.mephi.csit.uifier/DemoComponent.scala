package ru.mephi.csit.uifier

import Editable.ops._
import cats.effect.SyncIO
import outwatch.HtmlVNode
import outwatch.dsl._

final class DemoComponent(editor: Editor[Person]) {
  val node: HtmlVNode =
    div(
      h1("Редактор:"),
      editor.present,
      h1("Результат:"),
      div(editor.result.map(_.toString)),
    )
}

object DemoComponent {
  def init: SyncIO[DemoComponent] =
    for {
      editor <- Person("Иван", "Петров").editor
    } yield new DemoComponent(editor)
}
