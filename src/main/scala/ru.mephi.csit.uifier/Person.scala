package ru.mephi.csit.uifier

import cats.data.ValidatedNel
import cats.syntax.apply._
import colibri.Observable
import outwatch.VDomModifier
import outwatch.dsl._
import ru.mephi.csit.uifier.Editable.ops._

final case class Person(firstName: String, lastName: String)

object Person {

  val personPresentable: Presentable[Person] =
    Presentable(person => span(s"${person.firstName} ${person.firstName}"))

  implicit val personEditable: Editable[Person] =
    person =>
      for {
        firstNameEditor <- person.firstName.editor
        lastNameEditor  <- person.lastName.editor
      } yield
        new Editor[Person] {
          def result: Observable[ValidatedNel[String, Person]] =
            Observable.combineLatestMap(
              firstNameEditor.result,
              lastNameEditor.result,
            )(
              (
                firstNameValidated,
                lastNameValidated,
              ) => (firstNameValidated, lastNameValidated).mapN(Person.apply),
            )

          def present: VDomModifier =
            div(
              div("Имя:", firstNameEditor.present),
              div("Фамилия:", lastNameEditor.present),
            )
        }

}
